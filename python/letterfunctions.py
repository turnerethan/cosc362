"""Holds library of functions called in the letter writing main"""

class address:
  def __init__(self,first,last,adline1,adline2):
    self.first = first
    self.last = last
    self.adline1 = adline1
    self.adline2 = adline2
  def dumpInfo(self):
    print "Address Info:"
    print self.first, " ", self.last
    print "\n", self.adline1
    print "\n", self.adline2

