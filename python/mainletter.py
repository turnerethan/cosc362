""" script to do a mail merge to make custom letters to mythical beings
    [user@machine directory]$ python mainletter.py mailaddresslist.txt
"""
import sys

# read in commandline arguments
if len(sys.argv) != 2:
  print "To use: python mainletter.py mailaddresslist.txt"
else:
  print "Using addresses from file:", str(sys.argv[1])
  dataFile = str(sys.argv[1])

# open files
addressData = open(dataFile,"r") #open the file in read mode
from letterfunctions import *

# read data from files
print "Reading from file", dataFile

addressList=[]
for line in addressData.readlines():
  first, last, adline1, adline2 = map(str,line.split(',')
