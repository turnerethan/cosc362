#include <stdio.h>
#include <stdlib.h>
//this is a comment
/*
 * this is a multi line comment
 * C is a compiled language, so you have to manually compile it before you run it
 * the command to compile C code is:
 *   gcc [programname].c -o [executablefilename]
*/
int main(int argc, char *argv[]){
//to pass in arguments, take the count of args (argc) and the args themselves
//  in an array with a pointer (*argv[])
  printf("Hello World!\n");
  return 1;//all functions should return something
}
