#include <math.h>
/*
 *	Holds functions for use in pixel mapping
 */
int inCircle(int totalRows, int totalCols, int radius, int row, int col){
	int test = 0; //Int flag for if the given value is in the circle or not
								//1 = true, 0 = false
	int centerX = totalCols/2;	//center of the circle, calculated by 
	int centerY = totalRows/2;	//  size of the image
	int dist = 0;	//distance from the center to current pixel

	dist = pow(pow(centerX-col,2)+pow(centerY-row,2),0.5);
	if(dist<=radius){
		test=1;
	}
	return test;
}
