#include <stdio.h>
#include <stdlib.h>
/*
 * C program to create a red and blue plain pixel map
 * Usage: RedBluePPM [outfile] [numrows] [numcolumns]
*/
int main(int argc, char *argv[]){
  printf("Making a plain Pixel Map \n");

  int numRows;							//number of pixel rows in the image
  int numCols;							//number of pixel columns in the image
  int imageSize;						//total number of pixels used
	int row; int col;					//indexes used when writing image
  unsigned char *outImage;	//pointer to each pixel
	unsigned char *ptr; 			//fukken idk man
	//unsigned char *output;	//pointer to output file
	FILE *output;							//correct pointer to output file

  if(argc!=4){//check number of arguments
    printf("Usage: ./PixelMap OutFileName numrows numcols \n");
    exit(1);
  }
  if((numRows = atoi(argv[2])) <= 0){//check numRows is numeric
    printf("Error: numRows needs to be positive");
  }
  if((numCols = atoi(argv[3])) <= 0){//check numCols is numeric
    printf("Error: numCols needs to be positive");
  }
	//set up memory space for image
	imageSize = numRows*numCols*3; //each pixel needs space for an R, G, and B value
  outImage = (unsigned char *) malloc(imageSize);//allocate space in memory for image
	//set up output file
  if((output = fopen(argv[1], "w"))==NULL){ //try to open output file
		perror("output open error"); //print to error stream if file wasn't opened
		printf("Error: failed to open output file\n");
		exit(1);
	}
	//write out image
	ptr = outImage;
  for(row=0; row<numRows; row++){
		for(col=0; col<numCols; col++){
			if(col<numCols/2){//print red pixels in the first half of columns
				*ptr = 255;
				*(ptr+1)=0;
				*(ptr+2)=0;
			} else {//print blue pixels in the second half of columns
				*ptr = 0;
				*(ptr+1)=0;
				*(ptr+2)=255;
			}
			ptr+=3; //advance pointer
		}
	}
	//put info into a file with a header
	fprintf(output, "P6 %d %d 255\n", numCols, numRows);//write header
	fwrite(outImage, 1, imageSize, output);//write data
	//close file
	fclose(output);
	printf("Finished writing output\n");
  return 0;
}

