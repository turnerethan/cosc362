#!/usr/bin/python3
import sys
import os
import re

#COSC 362 Assignment 5
#This script reads multiple .log files and compiles each persons total recorded time into a report
def main():
#	if report file is not provided, print usage and exit
	if len(sys.argv) != 2:
		print("USAGE: python sumtime.py [report file].txt")
		sys.exit()

	print("-----Writing time report to " + str(sys.argv[1])+"-----")
	reportfile = open(sys.argv[1], "w")
#	search all files in current directory
	for file in os.listdir("."):
#		only open .log files
		if file.endswith(".log"):
			print("       Reading file " + str(file))
#			open .log file and begin parsing
			logfile=open(file)
			lines=logfile.readlines()
			minutes=0
#			parse each line
			for line in lines:
				writestring=""
				nums = getTimes(line)
#				if line contains 2 numbers, parse the 1st as hours, 2nd as mins, add to total
				if type(nums)==list:
					if len(nums)==2:
						minutes+=int(nums[0])*60+int(nums[1])
#				else, treat the line as a name and write it
				else:
					reportfile.write(line)
#			write total minutes to report and close
			reportfile.write("Total minutes: " + str(minutes) + "\n\n")
			logfile.close()
	reportfile.close()
	print("-----Finished writing report-----")

#Takes a string and returns any numeric values. If none are found, returns the string
#line: string to be parsed for numbers
#return: list of all numbers in line. If none are found, returns line
def getTimes(line):
#	use regular expressions to find numeric values
#	\d = all decimals, + = all decimals after another decimal
	nums=re.findall('\d+',line)
	if not nums:
		return line
	else:
		return nums

main()
