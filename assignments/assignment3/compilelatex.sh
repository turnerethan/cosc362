#!/bin/bash
#find .tex files
echo "*********************Files to be compiled****************************"
files=$(find . -name '*.tex')
echo $files
echo "************************Compiling Files******************************"
#compile .tex files
for file in $files
do
  pdflatex $file
done
#remove .aux, .log, .bib files
echo "*********************Removing extra files****************************"
rm *.aux *.log *.bib *.out
#report name and word count for each file into LatexCompileReport.txt
echo "************************Writing Report*******************************"
echo "Report compiled $(date)" > LatexCompileReport.txt
for file in $files
do
  echo $file >> LatexCompileReport.txt
  count=$(wc -w < $file)
  echo "Word count: "$count >>LatexCompileReport.txt
done
echo "*************************Job Complete*********************************"
