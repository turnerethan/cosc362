#include <stdio.h>
#include <stdlib.h>
#include "HelperFunctions.h"
#include "HelperFunctions2.h"
/*
 * C program to create a red and blue pixel map with a purple circle
 * Usage: PurpleCircle [outfile] [numrows] [numcolumns] [radius]
*/
int main(int argc, char *argv[]){
  printf("Making a plain Pixel Map \n");

  int numRows;              //number of pixel rows in the image
  int numCols;              //number of pixel columns in the image
	int radius;               //radius of the circle
  int imageSize;            //total number of pixels used
	int row; int col;         //indexes used when writing image
  unsigned char *outImage;
	unsigned char *ptr;       //pointer to each pixel
	FILE *output;							//pointer to output file

	//check variables
  if(argc!=5){//check number of arguments
    printf("Usage: ./main [OutFileName] [numrows] [numcols] [radius] \n");
		printf("Recommended values: numrows > 100, numcols = numrows, radius < numrows/2 \n");
    exit(1);
  }
  if((numRows = atoi(argv[2])) <= 0){//check numRows is numeric
    printf("Error: numRows needs to be positive");
		exit(1);
  }
  if((numCols = atoi(argv[3])) <= 0){//check numCols is numeric
    printf("Error: numCols needs to be positive");
		exit(1);
  }
	if((radius = atoi(argv[4])) <= 0){
		printf("Error: radius needs to be positive");
		exit(1);
	}

	//set up memory space for image
	imageSize = numRows*numCols*3; //each pixel needs space for an R, G, and B value
  outImage = (unsigned char *) malloc(imageSize);//allocate space in memory for image
	//set up output file
  if((output = fopen(argv[1], "w"))==NULL){ //try to open output file
		perror("output open error"); //print to error stream if file wasn't opened
		printf("Error: failed to open output file\n");
		exit(1);
	}

	//write out image
	ptr = outImage;
	//various values used when drawing different shapes
	//I put them here cos it looks better than defining them inside of the method call
	int centerX = numCols/2;
	int centerY = numRows/2;
	int stemLength = (0.25) * (radius*2);
	int leftCenterX = centerX-((0.33)*radius);
	int leftCenterY = centerY-((0.33)*radius);
	int leftRadius = (0.125)*radius;
	int rightCenterX = centerX+((0.33)*radius);
	int rightCenterY = centerY-((0.33)*radius);
	int rightRadius = (0.125)*radius;
	int mouthCenterX = centerX;
	int mouthCenterY = centerY+((0.5)*radius);
	int mouthRadius = (0.25)*radius;
	int noseCenterX = centerX;
	int noseCenterY = centerY;
	int noseLength = (0.125)*radius;

	for(row=0; row<numRows; row++){//traverse Y
		for(col=0; col<numCols; col++){//traverse X
			//print white background
			*ptr = 255;
			*(ptr+1)=255;
			*(ptr+2)=255;
			if(inSquare(centerX, centerY-radius, stemLength, row, col)){//draw stem
				*ptr = 0;
				*(ptr+1)=255;
				*(ptr+2)=0;
			}
			if(inCircle(centerX, centerY, radius, row, col)){//draw main circle
				*ptr = 255;
				*(ptr+1) = 155;
				*(ptr+2) = 0;
			}
			if (inCircle(leftCenterX, leftCenterY, leftRadius, row, col)){//in left eye
				*ptr = 0;
				*(ptr+1) = 0;
				*(ptr+2) = 0;
			} else if (inCircle(rightCenterX, rightCenterY, rightRadius, row, col)){//in right eye
				*ptr = 0;
				*(ptr+1) = 0;
				*(ptr+2) = 0;
			} else if (inCircle(mouthCenterX, mouthCenterY, mouthRadius, row, col)){//in mouth
				*ptr = 0;
				*(ptr+1) = 0;
				*(ptr+2) = 0;
			} else if (inSquare(noseCenterX, noseCenterY, noseLength, row, col)){//in nose
				*ptr = 0;
				*(ptr+1) = 0;
				*(ptr+2) = 0;
			}
			ptr+=3; //advance pointer
		}
	}

	//put info into a file with a header
	fprintf(output, "P6 %d %d 255\n", numCols, numRows);//write header
	fwrite(outImage, 1, imageSize, output);//write data
	//close file
	fclose(output);
	printf("Finished drawing a very surprised pumpkin (he's amazed that this program works)\n");
  return 0;
}
