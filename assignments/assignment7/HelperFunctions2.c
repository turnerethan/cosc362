#include <math.h>
/*
 *	Holds other functions for use in pixel mapping
 */
int inSquare(int centerX, int centerY, int length, int row, int col){
	if(col < centerX-(0.5*length)){
		return 0;
	} else if(col > centerX+(0.5*length)){
		return 0;
  } else if(row < centerY-(0.5*length)){
		return 0;
	} else if(row > centerY+(0.5*length)){
		return 0;
	} else {
		return 1;
	}
}
