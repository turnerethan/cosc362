Ethan Turner assignment 1
ls		Lists all items in the current directory except ones that start with .
ls -l		Formats displayed items to include more information, such as who last modified them and when
ls -a		Lists all items in a directory, including ones that start with .
#.name files?	I searched for a file with the extension .name and couldn't find anything so I'm guessing this is asking about
		files with . as the first character in the name
		. files are normally hidden from the user. They are usually configuration files that most users don't need to access
cd		Moves to the specified directory
cd ..		Moves up 1 folder in the file structure
rm -r		Removes a file recursively i.e. removing a directory removes all subdirectories
rm *.log	Removes all files with the extension .log
du		Displays disk usage
du -h		Displays disk usage in a way that is readable to humans
mv		Moves a file to a specified location
cp		Copies a file to a specified location
cp *.extension	Copies all files in a directory with the specified extension to a different directory
		cp omits directories by default unless the parameter -r is specified, which copies all directories and their contents
more <filename>	Displays the contents of a file
less <filename>	Displays the contents of a file but better
whoami		Displays the username of the user currently using the system
passwd		Changes the password of the current user
wc filename	Prints line, word, and byte count of a file

history, tail, and diff

tail		Reads the last lines of a file. The -n parameter specifies how many lines to read.
diff		Reads two files and returns the differences between them. In the case of history.log and historytail.log,
		it returned the first 138 lines of history.log and omitted the 20 lines in historytail.log
