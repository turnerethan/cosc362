9/10/2018
top - shows jobs currently being done by the system
	-u shows a specific user
last - shows past (successful) logins to the server
ctrl + c - quit a task if it stalls (eg top > somefile.txt)
kill <PID> - stops a task (works for gui things)

LaTeX
go to /cs362classchrispell/projects/ClassPresentations/ for examples
to compile: pdflatex <file>.tex - spits out a pdf file plus some other stuff

9/12/2018
pi todo:
passwd			change password from raspberry
apt-get update		you've already done this but might as well
apt-get upgrade 	lol remember when your pi ran wheezy
install tex-live 	view pdf's n shit
optional:
install gedit		alternative to nano, doesn't work in terminal
install emacs		idk what that is
install synaptic	gui apt-get
enable ssh		its in some gui options panel somewhere
			ifconfig in terminal to find address

9/14/2018
evince - open documents like .pdf

9/17/2018
~/.bashrc	automatically execute commands at start up, useful for setting up aliases
alias		use a different name to call the same command eg alias dog='cat'
export		??? idk man theres a $OLDPWD var

9/19/2018
the madman told everyone how to get on the wifi literally the day you figured it out wtf

mesg [y or n] 	turn on/off broadcast messages
wall, write	sends messages to other people

shell scripting
all scripts start with #!/bin/bash

read write permissions
read: 4
write: 2
exec: 1
permissions are displayed in ls -l as
[directoryornot][mine]-[groups]-[all]
changing this involves a lot of math and it's annoying
but to let you run a file as executable, the 1st number has to be 7 (you sum them up)
call to update permissions:
chmod [###] [yourfile]
eg chmod[744] - only you can execute the file, everyone else can only read

afterwards, it'll say -rwxrw-r-- 
to execute: ./[scriptname].sh

9/21/2018
Presentations - do a LaTeX writeup about your presentation 

shell scripts: put all of them in .local/bin
we don't know how to add more script locations yet lol

9/24/2018
see ~/.local/bin for scripting examples eg loop.sh, loop2.sh

10/1/2018
why do you skip class so much jesus

python - opens python shell

10/15
Setting up Jupyter Lab on pi

git clone https://github.com/kleinee/jns
this repository has a folder with a bunch of scripts in it
see the repository readme

10/17
started compiled languages
see C folder

10/19
Jupyter lab install - need to source a file via .bashrc so the server starts, see class repo

10/22
Final Project
idk man lol

11/5 moving files between linux machines
ftp - "file transfer protocol" good for moving 1 or 2 files at a time
sftp - ftp but with ssh ie secured transfer
	sftp [username]@[hostname]
	creates a connection to the remote machine
	can perform the same operations as you can locally
	local commands are specified with an l prefix eg lcd, lls
	get [remotefile] [localdestination] - download a thing
	mget - download multiple things
	put [localfile] [remotedestination] - upload to remote machine
	mput - upload multiple files
	exit - return to bash shell
	? or help - show all commands
rsync - "remote sync" good for moving tons of things at 1 time eg directories
	rsync [options] [source] [destination]
	options:
		-e: use ssh to connect
		-v: increase verbosity
		-r: operate recursively
		-a: archive mode (preserves symbolic links, permissions, and ownership)
		-h: present report in human readable format
		-z: compress files
		--progress: show a %done
		--dry-run: shows all language of the operations, but doesn't perform them
		--remove-source-files: delete files on the host machine as they are copied
		--include [param]: only affects files matching that parameter
			eg: include 'C*' -> only affects files starting with 'C'
		--exclude [param]: opposite of include
	source: directory files are being copied from
	destination: destiation files are being copied to
	eg: backup files to remote server
		rsnyc -zrh RemoteDirectory LocalBackup
all can be baked into bash scripts

11/7 rsync, tarballs
rsync and ssh - add "-e ssh" flag
	remoteuser@remoteserver.com:/file/path/to/directory
tar - Tape ARchive
	store a collection of directories and files into a single file
	create a tarball
		tar -cf -[other options] [output file].tar [directory to be archived]
	to extract from tar files
		tar -xf [filename].tar -C [output path]
	-c: create a new tar file
	-x: extract from tar file
	-v: verbose
	-f: use custom filename
	-z: compress files with gzip
	-j: compress files with bzip2 (smaller than gzip but takes longer)
	-t or --list: list all files in a tarball without extracting
	-r: append files to an existing tarball
	-w: verify a file is already in a tarball
	-d: show differences between an achive and current directory
	eg tar -cvf Archive.tar /unarchived/directory/
	filenames 
		tarballs end with .tar
		gzip'd tarballs end with .tar.gz or .tgz

11/12 cron jobs
r.i.p. stan lee
cron allows for execution of tasks at regular intervals
invoked thorugh command 'crontab'
	-l - list cron jobs
	-e - edit cron jobs
adding cron jobs
	each job takes 5 time options and the script to be run
	[min 0-59] [hrs 0-24] [days 1-7] [months 1-12] [weekday 0-6] [script].sh
	e.g. * * * * * script.sh - stars indicate to use defaults
	e.g. */5 * * * * script.sh - run script.sh every 5 minutes
	e.g. 30 15 * * 5 script.sh - run script.sh every Friday at 3:30pm (assuming 0 is Sunday)
	(*/ indicates every [this field] ie */1 means run every minute, 1 means run on the first minute
fancier example (that doesn't work lol)
	*/1 * * * * w > "~/cosc362/crontest/$(date '+%y-%m-%d').txt"
shortcuts
	@reboot [job]
	@yearly [job]
	@annualy [job]
	@monthly [job]
	@daily [job]
	@midnight [job]
	@hourly [job]
synergizes well with rsync:
	@daily rsync -avz pathtobeBackedup user@machine:/backup/destination

installing libraries
	sometimes you have to install a [B]ery [B]ig [B]ibrary
	most of them just tell you to use git
	after cloning, they have to be configured for your machine (painful) (aka fun)

